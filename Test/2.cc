// // Copyed from *THE* Paper

// struct node
// {
//     int data;
//     node *left, *right;
// };
// extern void *malloc(int __size);
// void insert(struct node **root, int data)
// {
//     if ((*root) == nullptr) {
//         *root = (struct node *)(sizeof(struct node));
//         (*root)->data = data;
//         (*root)->left = nullptr, (*root)->right = nullptr;
//     } else {
//         if (data < (*root)->data)
//             insert(&(*root)->left, data);
//         else
//             insert(&(*root)->right, data);
//     }
// }
#include<cstdio>
inline int read()
{
    char c=getchar();int x=0,f=1;
    while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
    while(c>='0'&&c<='9'){x=x*10+c-'0';c=getchar();}
    return x*f;
}