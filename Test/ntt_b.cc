// #include<cstdio>
// #define getchar() (p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 1<<21, stdin), p1 == p2) ? EOF : *p1++)
// #define swap(x,y) x ^= y, y ^= x, x ^= y
// #define LL long long 
// const int MAXN = 3 * 1e6 + 10, P = 998244353, G = 3, Gi = 332748118; 
// char buf[1<<21], *p1 = buf, *p2 = buf;
// inline int read() { 
//     char c = getchar(); int x = 0, f = 1;
//     while(c < '0' || c > '9') {if(c == '-') f = -1; c = getchar();}
//     while(c >= '0' && c <= '9') x = x * 10 + c - '0', c = getchar();
//     return x * f;
// }
// int N, M, limit = 1, L, r[MAXN];
// LL a[MAXN], b[MAXN];
// inline LL fastpow(LL a, LL k) {
// 	LL base = 1;
// 	while(k) {
// 		if(k & 1) base = (base * a ) % P;
// 		a = (a * a) % P;
// 		k >>= 1;
// 	}
// 	return base % P;
// }
// inline void NTT(LL *A, int type) {
// 	for(int i = 0; i < limit; i++) 
// 		if(i < r[i]) swap(A[i], A[r[i]]);
// 	for(int mid = 1; mid < limit; mid <<= 1) {	
// 		LL Wn = fastpow( type == 1 ? G : Gi , (P - 1) / (mid << 1));
// 		for(int j = 0; j < limit; j += (mid << 1)) {
// 			LL w = 1;
// 			for(int k = 0; k < mid; k++, w = (w * Wn) % P) {
// 				 int x = A[j + k], y = w * A[j + k + mid] % P;
// 				 A[j + k] = (x + y) % P,
// 				 A[j + k + mid] = (x - y + P) % P;
// 			}
// 		}
// 	}
// }
// int main() {
// 	N = read(); M = read();
// 	for(int i = 0; i <= N; i++) a[i] = (read() + P) % P;
// 	for(int i = 0; i <= M; i++) b[i] = (read() + P) % P;
// 	while(limit <= N + M) limit <<= 1, L++;
// 	for(int i = 0; i < limit; i++) r[i] = (r[i >> 1] >> 1) | ((i & 1) << (L - 1));	
// 	NTT(a, 1);NTT(b, 1);	
// 	for(int i = 0; i < limit; i++) a[i] = (a[i] * b[i]) % P;
// 	NTT(a, -1);	
// 	LL inv = fastpow(limit, P - 2);
// 	for(int i = 0; i <= N + M; i++)
// 		printf("%d ", (a[i] * inv) % P);
// 	return 0;
// }

#include<iostream>
#include<cstdio>
#include<cmath>
const double pi=acos(-1.0);
const int N=4e6+5;
using namespace std;
struct complex{
	double x,y;
	complex operator+(const complex &b)const{return (complex){x+b.x,y+b.y};}
	complex operator-(const complex &b)const{return (complex){x-b.x,y-b.y};}
	complex operator*(const complex &b)const{return (complex){x*b.x-y*b.y,x*b.y+y*b.x};}
}a[N];
int rev[N];
inline char gc()
{
	static char buf[1<<16],*S,*T;
	if(S==T)
	{
		T=(S=buf)+fread(buf,1,1<<16,stdin);
		if(S==T)return EOF;
	}
	return *(S++);
}
#define getchar gc
inline int read()
{
	char h=getchar();
	int y=0;
	while(h<'0'||h>'9')h=getchar();
	while(h>='0'&&h<='9')y=y*10+h-'0',h=getchar();
	return y;
}
inline void fft(complex a[],int n,int ty)
{
	for(int i=0;i<n;i++)if(i<rev[i])swap(a[i],a[rev[i]]);
	for(int l=1;l<n;l<<=1)
	for(int i=0;i<n;i+=(l<<1))
	{
		complex res=(complex){1,0},wn=(complex){cos(pi/l),ty*sin(pi/l)};
		for(int j=0;j<l;j++,res=res*wn)
		{
			complex x=a[i+j],y=res*a[i+j+l];
			a[i+j]=x+y;
			a[i+j+l]=x-y;
		}
	}
}
int main()
{
	int n=read(),m=read();
	for(int i=0;i<=n;i++)a[i].x=read();
	for(int i=0;i<=m;i++)a[i].y=read();
	int s=0,res=0;
	while((1<<res)<n+m+1)res++;
	s=1<<res;
	for(int i=0;i<s;i++)rev[i]=(rev[i>>1]>>1)|((i&1)<<(res-1));
	fft(a,s,1);
	for(int i=0;i<s;i++)a[i]=a[i]*a[i];
	fft(a,s,-1);
	for(int i=0;i<=n+m;i++)cout<<(int)(a[i].y/s/2+0.5)<<" ";
}