#include "alignment.hpp"
#include "gtest/gtest.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/raw_os_ostream.h"

constexpr int64_t m = 1, d = -1, g = -2;

static auto GetAlignment(const std::vector<std::string>& SeqX, const std::vector<std::string>& SeqY, int64_t m = ::m,
                         int64_t d = ::d, int64_t g = ::g)
{
    /*
        The optimal alignment can be calculated by the Needleman-Wunsch algorithm. Parameters
        m, d, g of a score function w are required restrictions such that m = −g and m ≤ d ≤ g.
    */
    NeedlemanWunsch<llvm::StringRef> NW{/*m=*/m, /*d=*/d, /*g=*/g, /*gap=*/"-"};
    std::vector<llvm::StringRef> X, Y;
    for (const auto& x : SeqX) X.emplace_back(x);
    for (const auto& y : SeqY) Y.emplace_back(y);
    const auto DP = NW.DP(X, Y);
    for (size_t i = 0; i <= DP.size() - 1; ++i)
        for (size_t j = 0; j <= DP[0].size() - 1; ++j) llvm::outs() << DP[i][j] << " \n"[j == DP[0].size() - 1];
    return NW.Alignment(X, Y, DP);
}

static auto MakeSeq(llvm::StringRef S)
{
    std::vector<std::string> Ret;
    for (auto c : S) Ret.emplace_back(std::string(1, c));
    return Ret;
}

static auto MapSeq(auto& S, llvm::StringRef sep = "", llvm::StringRef quota = "")
{
    return llvm::join(llvm::map_range(S, [sep, quota](auto& X) -> std::string { return (quota + X + quota).str(); }),
                      sep);
}

TEST(Alignment, Simple)
{
    const static std::string X = "masters";
    const static std::string Y = "stars";
    const auto& Alignment = GetAlignment(MakeSeq(X), MakeSeq(Y));
    llvm::outs() << MapSeq(Alignment[0]) << "\n" << MapSeq(Alignment[1]) << "\n";
    ASSERT_TRUE(MapSeq(Alignment[0]) == "masters");
    ASSERT_TRUE(MapSeq(Alignment[1]) == "--stars");
}
