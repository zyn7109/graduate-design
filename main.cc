// Implementation for
// _A Source Code Plagiarism Detecting Method Using Alignment with Abstract Syntax Tree Elements_

#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/FileSystemOptions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/CompilerInvocation.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/Utils.h"
#include "clang/Serialization/PCHContainerOperations.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/JSONCompilationDatabase.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/Hashing.h"
#include "llvm/ADT/None.h"
#include "llvm/ADT/STLExtras.h"
#if LLVM_VERSION_MAJOR >= 14
#include "llvm/ADT/STLFunctionalExtras.h"
#endif
#include "llvm/ADT/SmallString.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringExtras.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Demangle/Demangle.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/Format.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/JSON.h"
#include "llvm/Support/Path.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/VirtualFileSystem.h"
#include "llvm/Support/WithColor.h"
#include "llvm/Support/raw_ostream.h"

#include <cstdlib>
#include <limits>
#include <string>
#include <string_view>
#include <iterator>

#include "alignment.hpp"
#include "ast.h"

namespace {

using llvm::cl::cat;
using llvm::cl::desc;
using llvm::cl::init;
using llvm::cl::list;
using llvm::cl::opt;
using llvm::cl::OptionCategory;

OptionCategory option_category("scpd");

opt<std::string> compile_commands{
    "opt",
    cat(option_category),
    desc("compile_commands.json 的位置"),
};

opt<std::string> dump_json{
    "dump-json",
    cat(option_category),
};

list<std::string> file{
    "file",
    cat(option_category),
    desc("源文件"),
};

opt<std::string> debug{
    "debug",
    llvm::cl::Hidden,
};

opt<std::string> gap{
    "sep",
    init("-"),
    cat(option_category),
    desc("nw 分隔符，默认是 -"),
};

opt<int64_t> m{
    "m",
    cat(option_category),
    desc("nw 参数 m"),
    init(1),
};

opt<int64_t> d{
    "d",
    cat(option_category),
    desc("nw 参数 d"),
    init(-1),
};

opt<int64_t> g{
    "g",
    cat(option_category),
    desc("nw 参数 g"),
    init(-2),
};

static auto JoinSequence = [](const auto& Ret) {
    llvm::json::Array A;
    llvm::transform(Ret, std::back_inserter(A), [] (auto& X) {
        llvm::json::Object O, Range, Start, End;
        Start.try_emplace("line", X.BeginLine);
        Start.try_emplace("column", X.BeginCol);
        End.try_emplace("line", X.EndLine);
        End.try_emplace("column", X.EndCol);
        Range.try_emplace("start", std::move(Start));
        Range.try_emplace("end", std::move(End));
        O.try_emplace("node", X.ASTNodeName);
        O.try_emplace("range", std::move(Range));
        return O;
    });
    return A;
};

}  // namespace


struct TokenSequence
{
    struct ASTElement
    {
        std::string ASTNodeName;
        uint64_t BeginLine, BeginCol;
        uint64_t EndLine, EndCol;

        friend bool operator==(const TokenSequence::ASTElement& LHS, const TokenSequence::ASTElement& RHS)
        {
            return LHS.ASTNodeName == RHS.ASTNodeName;
        }

        ASTElement(llvm::StringRef Name = ::gap, uint64_t BeginLine = -1, uint64_t BeginCol = -1, uint64_t EndLine = -1,
                   uint64_t EndCol = -1)
            : ASTNodeName(Name.str()), BeginLine(BeginLine), BeginCol(BeginCol), EndLine(EndLine), EndCol(EndCol)
        {
        }
    };

    static void RecursiveVisitASTJSONPreOrder(const clang::SourceManager& SM, const llvm::json::Array& Array, std::vector<ASTElement>& Ret)
    {
        for (auto& Node : Array) {
            const auto Obj = Node.getAsObject();
            if (!Obj) return;
            if (Obj->getBoolean("isImplicit").getValueOr(false)) continue;
            const auto Kind = Obj->getString("kind").getValueOr("<empty-kind>");
            const auto Name = Obj->getString("name").getValueOr("");
            const auto Range = Obj->getObject("range");
            if (!Range) continue;
            const auto Begin = Range->getObject("begin");
            const auto End = Range->getObject("end");
            const auto BeginOffset = Range->getObject("begin")->getNumber("offset").getValueOr(0);
            const auto EndOffset = Range->getObject("end")->getNumber("offset").getValueOr(0);
            std::string Str = Kind.str();
            if (!Name.empty()) Str += (" " + Name).str();
            Ret.emplace_back(std::move(Str), Begin->getInteger("line").getValueOr(SM.getLineNumber(SM.getMainFileID(), BeginOffset)),
                             Begin->getInteger("col").getValueOr(0), End->getInteger("line").getValueOr(SM.getLineNumber(SM.getMainFileID(), EndOffset)),
                             End->getInteger("col").getValueOr(0));
            if (const auto inner = Obj->getArray("inner"); inner) {
                RecursiveVisitASTJSONPreOrder(SM, *inner, Ret);
            }
        }
    }

    static std::vector<llvm::StringRef> ConvertToRefVector(const std::vector<std::string>& X)
    {
        return {X.begin(), X.end()};
    }

    static auto GetAlignment(NeedlemanWunsch<TokenSequence::ASTElement> NW, const std::vector<TokenSequence::ASTElement>& SeqX,
                             const std::vector<TokenSequence::ASTElement>& SeqY)
    {
        // NeedlemanWunsch NW{/*m=*/::m, /*d=*/::d, /*g=*/::g};
        // std::vector<llvm::StringRef> X = ConvertToRefVector(SeqX), Y = ConvertToRefVector(SeqY);
        const auto DP = NW.DP(SeqX, SeqY);
        return NW.Alignment(SeqX, SeqY, DP);
    }

    static bool IsGap(const ASTElement& E) { return llvm::StringRef(E.ASTNodeName).contains(::gap); }

    static llvm::Optional<llvm::StringRef> IdentifierOf(const ASTElement& E)
    {
        const auto& X = E.ASTNodeName;
        if (IsGap(E)) {
            return llvm::None;
        }
#if LLVM_VERSION_MAJOR >= 14
        const auto Range = llvm::split(X, ' ');
        if (std::distance(Range.begin(), Range.end()) == 2) {
            const auto Id = *(++Range.begin());
            // llvm::errs() << "\033[33mId: " << Id << "\033[0m\n";
            return Id;
        }
#else
        llvm::SmallVector<llvm::StringRef> Ret;
        llvm::SplitString(X, Ret, " ");
        if (Ret.size() == 2) {
            const auto Id = Ret[1];
            // llvm::errs() << "\033[33mId: " << Id << "\033[0m\n";
            return Id;
        }
#endif
        return llvm::None;
    }

    static auto GetInitialAlignment(const std::vector<TokenSequence::ASTElement>& X, const std::vector<TokenSequence::ASTElement>& Y)
    {
        NeedlemanWunsch<TokenSequence::ASTElement> OriginNW{/*m=*/::m, /*d=*/::d, /*g=*/::g, /*gap=*/};
        const auto Alignment = GetAlignment(OriginNW, X, Y);
        return Alignment;
    }

    static auto GetFunctionSequenceAlignmentScore(llvm::StringRef NameOfX, llvm::StringRef NameOfY,
                                                  const std::vector<TokenSequence::ASTElement>& X,
                                                  const std::vector<TokenSequence::ASTElement>& Y,
                                                  llvm::Optional<llvm::json::Array>& OutputJson)
    {
        const auto Alignment = GetInitialAlignment(X, Y);
        if (&X != &Y && OutputJson) {
            llvm::json::Object O;
            O.try_emplace((NameOfX + "@LHS").str(), JoinSequence(Alignment[0]));
            O.try_emplace((NameOfY + "@RHS").str(), JoinSequence(Alignment[1]));
            OutputJson->emplace_back(std::move(O));
        }
        /*
            The optimal alignment can be calculated by the Needleman-Wunsch algorithm. Parameters
            m, d, g of a score function w are required restrictions such that m = −g and m ≤ d ≤ g.
        */
        NeedlemanWunsch<TokenSequence::ASTElement> ILAR_NW{::m, ::d, ::g, /*gap=*/{},
                                [](const TokenSequence::ASTElement& x, const TokenSequence::ASTElement& y, int64_t m, int64_t d, int64_t g) -> int64_t {
                                    if (const auto P = IdentifierOf(x), Q = IdentifierOf(y); P && Q && P == Q) {
                                        // return 2;
                                        return m;
                                    } else if (!P && !Q && x == y && !IsGap(x) && !IsGap(y)) {
                                        // return 1;
                                        return d;
                                    } else if ((!P || !Q) && x != y && (IsGap(x) || IsGap(y))) {
                                        return g;
                                    } else if (P && Q && x != y) {
                                        return 0;
                                    } else if (!P && !Q && x != y && !IsGap(x) && !IsGap(y)) {
                                        return -m;
                                    } else {
                                        // llvm::errs() << "\033[31m" << x << " " << y << "\033[0m\n";
                                        return x == y ? d : -d;
                                        // assert(0 && "You should not be here!");
                                    }
                                }};
        const auto Score = ILAR_NW.DP(Alignment[0], Alignment[1]);
        // for (size_t i = 0; i < Score.size(); ++i)
        //     for (size_t j = 0; j < Score[0].size(); ++j)
        //         llvm::outs() << llvm::formatv("{0,5:d}", Score[i][j]) << " \n"[j == Score[0].size() - 1];
        return Score[Score.size() - 1][Score[0].size() - 1];
    }

    static auto ILAR_NormalizedScore(llvm::StringRef NameOfX, llvm::StringRef NameOfY,
                                     const std::vector<TokenSequence::ASTElement>& X,
                                     const std::vector<TokenSequence::ASTElement>& Y,
                                     llvm::Optional<llvm::json::Array>& OutputArray)
    {
        const auto XY = GetFunctionSequenceAlignmentScore(NameOfX, NameOfY, X, Y, OutputArray);
        const auto XX = GetFunctionSequenceAlignmentScore(NameOfX, NameOfY, X, X, OutputArray);
        const auto YY = GetFunctionSequenceAlignmentScore(NameOfX, NameOfY, Y, Y, OutputArray);
        // llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::CYAN)
        //     << llvm::formatv("{0} {1} {2}\n", XY, XX, YY);
        return 0.5 * (1 + (2.0 * XY) / (XX + YY));
    }
};

int main(int argc, char* argv[])
{
    llvm::sys::PrintStackTraceOnErrorSignal(argv[0]);
    llvm::cl::ParseCommandLineOptions(argc, argv);
    std::unique_ptr<clang::tooling::CompilationDatabase> database;
    if (std::string err_msg;
        (database = clang::tooling::JSONCompilationDatabase::loadFromFile(
             compile_commands, err_msg, clang::tooling::JSONCommandLineSyntax::AutoDetect)) == nullptr) {
        llvm::report_fatal_error(err_msg.c_str());
    }

    auto FS = llvm::vfs::getRealFileSystem();
    // clang::FileManager FM{clang::FileSystemOptions(), FS};
    llvm::SmallVector<llvm::StringRef> SourceFileNameVector;
    // const auto& ACC = database->getAllCompileCommands();
    // SourceFileNameVector.reserve(ACC.size());
    // for (const auto& CC : ACC) SourceFileNameVector.emplace_back(CC.Filename);

    const auto DO = ::new clang::DiagnosticOptions;
    DO->ShowColors = 1;
    const auto D = clang::CompilerInstance::createDiagnostics(DO, nullptr, false);
    llvm::StringMap<std::shared_ptr<clang::CompilerInvocation>> CIs;
    for (const auto& File : file) {
        const auto AbsPath = std::move(clang::tooling::getAbsolutePath(*FS, File).get());
        if (std::vector<clang::tooling::CompileCommand> C = database->getCompileCommands(AbsPath); C.empty()) {
            llvm::report_fatal_error(
                llvm::formatv("cannot find CompileCommand for `{0}' from `{1}'", File, compile_commands));
        } else {
            for (auto& CC : C) {
                llvm::SmallVector<const char*, 32> Ref;
                for (auto& C : CC.CommandLine) Ref.emplace_back(C.c_str());
                CIs.try_emplace(CC.CommandLine[1], clang::createInvocationFromCommandLine(Ref, D));
            }
        }
    }

    llvm::SmallVector<std::shared_ptr<clang::CompilerInstance>, 16> Instances;
    for (auto& I : CIs) {
        SourceFileNameVector.emplace_back(I.first());
        auto& CI = I.second;
        auto Compiler = std::make_shared<clang::CompilerInstance>();
        Compiler->setInvocation(CI);
        Compiler->createDiagnostics(D->getClient(), false);
        // llvm::IntrusiveRefCntPtr<llvm::vfs::FileSystem> VFS;
        // if (VFS = clang::createVFSFromCompilerInvocation(*CI, *D); !VFS) {
        //     llvm::errs() << "failed to create VFS from CI" << "\n";
        //     continue;
        // }
        if (!Compiler->createFileManager(FS)) {
            llvm::report_fatal_error(llvm::formatv(
                "failed to create FileManager for CompilerInstance created by commands `{0}'", I.first()));
            continue;
        }
        if (!Compiler->createTarget()) {
            llvm::report_fatal_error(
                llvm::formatv("failed to create Target for CompilerInstance created by commands `{0}'", I.first()));
            continue;
        }
        Instances.emplace_back(std::move(Compiler));
    }

    std::map<std::shared_ptr<clang::CompilerInstance>, std::pair<llvm::json::Value, std::string>> ASTJson;
    llvm::StringSet<> SourceFileNames;
    SourceFileNames.insert(SourceFileNameVector.begin(), SourceFileNameVector.end());
    for (auto I : llvm::zip(Instances, SourceFileNameVector)) {
        std::string buffer;
        llvm::raw_string_ostream RSO(buffer);
        RSO << R"({"tree":[)";
        ASTSequenceAction Action{RSO, SourceFileNames};
        std::get<0>(I)->ExecuteAction(Action);
        RSO << R"(]})";
        ASTJson.try_emplace(std::get<0>(I), std::move(llvm::json::parse(buffer).get()), std::get<1>(I));
    }

    using FunctionTokenSequenceType = std::vector<std::vector<TokenSequence::ASTElement>>;
    std::vector<FunctionTokenSequenceType> Seqs;
    llvm::SmallVector<llvm::StringRef> FileNameHeader;
    std::vector<std::vector<std::string>> FuncDecl;
    for (auto& AST : ASTJson) {
        FileNameHeader.emplace_back(AST.second.second);
        const auto Tree = *AST.second.first.getAsObject()->getArray("tree");
        llvm::json::Array A{llvm::make_filter_range(Tree, [](const auto& Element) {
            return Element.getAsObject()->getString("kind").getValueOr("") == "FunctionDecl";
        })};
        Seqs.emplace_back();
        FuncDecl.emplace_back();
        // 当前翻译单元中的每一个函数 Decl
        for (const auto& E : A) {
            auto& Current = Seqs.back();
            auto& CurrentFuncDecl = FuncDecl.back();
            Current.emplace_back();
            CurrentFuncDecl.emplace_back(E.getAsObject()->getString("mangledName").getValueOr("<FuncNameEmpty>"));
            TokenSequence::RecursiveVisitASTJSONPreOrder(AST.first->getSourceManager(), llvm::json::Array{E}, Current.back());
        }
    }

    assert(Seqs.size() == 2 && "Only 2 sources are supported!");

    /*
        Select one token sequence divided in function units
        from S1 , and calculate the alignment between the
        selected token sequence and each token sequence of
        S2 . Continue calculating the alignments between each
        left token sequence and each token sequence of S2 .
        Then, normalize using the obtained alignment score.
    */
    std::vector<double> Scores;
    const size_t Size = Seqs[0].size() * Seqs[1].size();
    Scores.reserve(Size);

    double Average = 0, Max = 0;
    if (Size) {
        const auto A = llvm::sys::path::filename(FileNameHeader[0]);
        const auto B = llvm::sys::path::filename(FileNameHeader[1]);
        llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::GREEN)
            << llvm::formatv("{0,-30} {1,-30} {2:7}", A, B, "Score") << "\n";
    }

    static auto DemangleFuncName = [](llvm::StringRef MangledName) -> std::string {
        int status;
        size_t n;
        static auto Deleter = [](char* P) { free(P); };
#ifndef _MSC_VER
        std::unique_ptr<char, decltype(Deleter)> PtrBuffer{
            llvm::itaniumDemangle(MangledName.data(), nullptr, &n, &status), Deleter};
#else
        std::unique_ptr<char, decltype(Deleter)> PtrBuffer{
            llvm::microsoftDemangle(MangledName.data(), nullptr, nullptr, &status, nullptr), Deleter};
#endif
        if (status == llvm::demangle_success) return {PtrBuffer.get()};
        return MangledName.str();
    };

    llvm::Optional<llvm::raw_fd_ostream> DumpJSONFile;
    llvm::Optional<llvm::json::Array> Array;
    if (!dump_json.empty()) {
        std::error_code EC;
        DumpJSONFile.emplace(dump_json, EC);
        llvm::json::Array array;
        if (EC) {
            llvm::report_fatal_error(llvm::formatv("cannot open file {0}", dump_json), false);
        }
        Array = std::move(array);
    }

    for (auto& S1 : llvm::enumerate(Seqs[0])) {
        for (auto& S2 : llvm::enumerate(Seqs[1])) {
            const auto NameOfX = DemangleFuncName(FuncDecl[0][S1.index()]);
            const auto NameOfY = DemangleFuncName(FuncDecl[1][S2.index()]);
            const auto Score = TokenSequence::ILAR_NormalizedScore(NameOfX, NameOfY, S1.value(), S2.value(), Array);
            Scores.emplace_back(Score);
            llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::YELLOW)
                << llvm::formatv("{0,-30} {1,-30}", NameOfX, NameOfY);
            llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::MAGENTA, false, false) << llvm::formatv("{0:f5}", Score) << "\n";
            Average += Score / Size;
            Max = std::max(Max, Score);
        }
    }
    if (DumpJSONFile && Array) {
        llvm::json::OStream JOS{*DumpJSONFile};
        llvm::json::Value V(std::move(*Array));
        JOS.value(V);
    }
    /*
        For each alignment score between methods S1 and
        S2 , find the combination that has the maximum score.
        Then calculate the average score for the obtained
        value of the combination.
    */
    llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::BLUE, true)
        << llvm::formatv("Average: {0:f5}\nMax: {1:f5}", Average, Max) << "\n";

    // Bug: clang::tooling::ClangTool@Tooling.cpp injectResourceDir 假定这个工具和 clang 一个目录
    // llvm-project/llvm/lib/Support/Unix/Path.inc:getMainExecutable
    // llvm::outs() << "\033[31m" << FS->getCurrentWorkingDirectory().get() << "\033[0m\n";
    // clang::tooling::ClangTool clang_tool{*database, file, std::make_shared<clang::PCHContainerOperations>(), FS};
    // clang_tool.appendArgumentsAdjuster(clang::tooling::getInsertArgumentAdjuster("-v"));
    // const auto action = clang::tooling::newFrontendActionFactory<clang::SyntaxOnlyAction>();
    // clang_tool.run(action.get());

    // FIXME(sonnyzhang): invalid free segment fault
    // const auto action = clang::tooling::newFrontendActionFactory<clang::SyntaxOnlyAction>();
    // for (const auto& File : file) {
    //     if (std::vector<clang::tooling::CompileCommand> C = database->getCompileCommands(File); C.empty()) {
    //         llvm::errs() << llvm::formatv("cannot find compile command for {0} from {1}", File, compile_commands);
    //     } else {
    //         for (auto& CC : C) {
    //             llvm::SmallVector<const char*, 32> Ref;
    //             for (auto& C : CC.CommandLine) Ref.emplace_back(C.c_str());
    //             clang::tooling::ToolInvocation TI{CC.CommandLine, action.get(), &FM,
    //                                               std::make_shared<clang::PCHContainerOperations>()};
    //             TI.run();
    //         }
    //     }
    // }
}
