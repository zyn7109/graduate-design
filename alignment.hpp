#pragma once

#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/Hashing.h"
#include "llvm/ADT/None.h"
#include "llvm/ADT/STLExtras.h"
#if LLVM_VERSION_MAJOR >= 14
#include "llvm/ADT/STLFunctionalExtras.h"
#endif
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringExtras.h"

template <typename Element>
struct NeedlemanWunsch
{
    std::vector<std::vector<int64_t>> DP(llvm::ArrayRef<Element> S, llvm::ArrayRef<Element> T) const;

    std::vector<std::vector<Element>> Alignment(llvm::ArrayRef<Element> S,
                                                    llvm::ArrayRef<Element> T,
                                                    const std::vector<std::vector<int64_t>>& Score) const;

    int64_t m;
    int64_t d;
    int64_t g;
    Element gap;
    llvm::function_ref<int64_t(const Element&, const Element&, int64_t /*m*/, int64_t /*d*/, int64_t /*g*/)> W =
        [](const Element& x, const Element& y, int64_t m, int64_t d, int64_t g) {
            if (x == y) return m;
            return d;
        };

    int64_t MinThreshold = std::numeric_limits<int64_t>::min();
};

template <typename Element>
std::vector<std::vector<int64_t>> NeedlemanWunsch<Element>::DP(llvm::ArrayRef<Element> S,
                                                               llvm::ArrayRef<Element> T) const
{
    if (S.size() < T.size()) std::swap(S, T);
    const auto M = S.size(), N = T.size();
    // Assume |S| >= |T|
    std::vector<std::vector<int64_t>> D(N + 1, std::vector<int64_t>(M + 1, std::numeric_limits<int64_t>::min()));
    for (size_t i = 0; i <= N; ++i) D[i][0] = i * g;
    for (size_t i = 0; i <= M; ++i) D[0][i] = i * g;
    for (size_t i = 1; i <= N; ++i)
        for (size_t j = 1; j <= M; ++j)
            D[i][j] = std::max({
                MinThreshold,
                D[i - 1][j - 1] + W(S[j - 1], T[i - 1], m, d, g),
                D[i - 1][j] + g,
                D[i][j - 1] + g,
            });
    return D;
}

template <typename Element>
std::vector<std::vector<Element>> NeedlemanWunsch<Element>::Alignment(
    llvm::ArrayRef<Element> S, llvm::ArrayRef<Element> T, const std::vector<std::vector<int64_t>>& Score) const
{
    const bool SwapFlag = S.size() < T.size();
    if (SwapFlag) std::swap(S, T);
    // Assume |S| >= |T|
    std::vector<std::vector<Element>> Ret(2, std::vector<Element>{});
    for (size_t i = T.size(), j = S.size(); i || j;) {
        auto &A = Ret[0], &B = Ret[1];
        if (!i) {
            A.emplace_back(S[j - 1]);
            B.emplace_back(gap);
            --j;
            continue;
        }
        if (!j) {
            A.emplace_back(gap);
            B.emplace_back(T[i - 1]);
            --i;
            continue;
        }
        if (Score[i][j] == Score[i - 1][j - 1] + W(S[j - 1], T[i - 1], m, d, g)) {
            A.emplace_back(S[j - 1]), B.emplace_back(T[i - 1]);
            --j, --i;
        } else if (Score[i][j] == Score[i - 1][j] + g) {
            A.emplace_back(gap), B.emplace_back(T[i - 1]);
            --i;
        } else if (Score[i][j] == Score[i][j - 1] + g) {
            A.emplace_back(S[j - 1]), B.emplace_back(gap);
            --j;
        }
    }
    if (SwapFlag) std::swap(Ret[0], Ret[1]);
    decltype(Ret) R{2};
    {
        const auto It = llvm::reverse(Ret[0]);
        R[0] = {It.begin(), It.end()};
    }
    {
        const auto It = llvm::reverse(Ret[1]);
        R[1] = {It.begin(), It.end()};
    }
    return R;
}