#pragma once

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Decl.h"
#include "clang/AST/JSONNodeDumper.h"
#include "clang/AST/PrettyPrinter.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Basic/LangOptions.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/WithColor.h"
#include "llvm/Support/raw_ostream.h"

#include <memory>

class ASTPrinter : public clang::ASTConsumer, public clang::RecursiveASTVisitor<ASTPrinter>
{
   public:
    explicit ASTPrinter(clang::ASTContext &C, llvm::raw_ostream &OS, const llvm::StringSet<> &SourceSet) noexcept
        : TUASTContext(C), ROS(OS), AvailableSource(SourceSet)
    {
    }

    ~ASTPrinter() = default;

    virtual void HandleTranslationUnit(clang::ASTContext &Ctx) override
    {
        JSONDumper = std::make_unique<clang::JSONDumper>(ROS, TUASTContext.getSourceManager(), TUASTContext,
                                                         clang::PrintingPolicy{clang::LangOptions{}}, nullptr);
        clang::RecursiveASTVisitor<ASTPrinter>::TraverseTranslationUnitDecl(Ctx.getTranslationUnitDecl());
    }

    bool NeedSkip(llvm::StringRef File)
    {
        return File.empty() || !AvailableSource.contains(File);
    }

    bool TraverseDecl(clang::Decl *D)
    {
        if (D->isInStdNamespace()) return true;
        const auto &SM = TUASTContext.getSourceManager();
        const auto &Range = D->getSourceRange();
        const auto FileName_1 = SM.getFilename(Range.getBegin());
        const auto FileName_2 = SM.getFilename(Range.getEnd());
        // if (FileName_1 != FileName_2) {
        //     llvm::WithColor(llvm::errs(), llvm::raw_ostream::RED) << FileName_1 << " " << FileName_2 << "\n";
        //     assert(FileName_1 == FileName_2 && "Decl is not in one File!");
        // }
        if (NeedSkip(FileName_1)) {
            // llvm::errs() << "Skipped " << FileName_1 << "\n";
            return true;
        }
        if (NeedSkip(FileName_2)) {
            // llvm::errs() << "Skipped " << FileName_2 << "\n";
            return true;
        }
        JSONDumper->Visit(D);
        return true;
    }

   private:
    clang::ASTContext &TUASTContext;
    std::unique_ptr<clang::JSONDumper> JSONDumper;
    llvm::raw_ostream &ROS;
    const llvm::StringSet<> &AvailableSource;
};

class ASTSequenceAction : public clang::ASTFrontendAction
{
   public:
    ASTSequenceAction(llvm::raw_ostream &OS, const llvm::StringSet<> &SourceSet) : ROS(OS), SourceSet(SourceSet) {}

   protected:
    std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &CI, llvm::StringRef InFile) override
    {
        return std::make_unique<ASTPrinter>(CI.getASTContext(), ROS, SourceSet);
    }

   private:
    llvm::raw_ostream &ROS;
    const llvm::StringSet<> &SourceSet;
};
