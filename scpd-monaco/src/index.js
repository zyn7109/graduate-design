import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { AppBar, Button, CardContent, Card, Container, Divider, InputLabel, Input, List, FormHelperText, FormControl, Box, Stack, Toolbar, Typography, CardHeader, ListItemText, ListItemButton, Grid } from '@mui/material';
import Editor from '@monaco-editor/react'
import * as monaco from 'monaco-editor'
import Exposition from './exposition.json'

function LoadJSON() {
  return Exposition
}

function getFunction(string) {
  const JSON = LoadJSON()
  let Ret = new Set()
  for (const i in JSON) {
    for (const vv in JSON[i]) {
      if (vv.endsWith(string))
        Ret.add(vv)
    }
  }
  return Array.from(Ret)
}

let Functions = [getFunction("LHS"), getFunction("RHS")]
let Selected = [-1, -1]

function getFnPair(LHS, RHS) {
  const JSON = LoadJSON()
  let Ret = []
  console.log(LHS, RHS)
  for (const i in JSON) {
    const Arr = Object.entries(JSON[i])
    if (Arr[0][0] == RHS && Arr[1][0] == LHS)
      [Arr[0], Arr[1]] = [Arr[1], Arr[0]]
    if (Arr[0][0] == LHS && Arr[1][0] == RHS) {
      Ret.push(Arr[0][1])
      Ret.push(Arr[1][1])
    }
  }
  return Ret
}

class FunctionList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      select: null
    }
  }
  render() {
    const { functions } = this.props
    return (
      <List sx={{ flexGrow: 1 }}>
        {functions.map((value, index) => {
          value = `${value.replace('@LHS', '').replace('@RHS', '')}`
          return (
            <ListItemButton key={index} onClick={() => {
              this.setState({ select: index })
              Selected[this.props.which] = index
              // console.log(Selected)
              this.props.onReady()
            }} selected={index == this.state.select}>
              <ListItemText primary={value} />
            </ListItemButton>
          )
        })}
      </List>
    )
  }
}

class Block extends React.Component {
  constructor(props) {
    super(props)
    this.editor = React.createRef()
    this.monaco = React.createRef()
    this.state = {
      codeName: this.props.DefaultCodeName,
      compileCommands: null,
      editor: null,
      monaco: null,
    }
  }

  getState(callback) {
    this.setState((state) => callback(state))
  }

  handleEditorDidMount(Editor, Monaco) {
    this.editor.current = Editor
    this.monaco.current = Monaco
  }

  onCompileCommandsChange(event) {
    this.setState(() => ({
      compileCommands: event.target.value
    }))
  }

  render() {
    return (
      <Container>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          {this.state.codeName}
        </Typography>
        <Divider orientation="horizontal" flexItem />
        <FormControl sx={{ marginTop: 2 }} fullWidth>
          <InputLabel htmlFor="command-1">编译命令</InputLabel>
          <Input id="command-1" aria-describedby="command-1-help" onChange={(event) => this.onCompileCommandsChange(event)} autoComplete="off" />
          <FormHelperText id="command-1-help">例如: clang++ test.cc -std=c++11 -o test.o</FormHelperText>
          <Container sx={{ marginTop: 2 }}>
            <Editor defaultLanguage="cpp" height="60vh"
             value="#include <iostream>"
            id="monaco-editor" onMount={(editor, monaco) => this.handleEditorDidMount(editor, monaco)}>
            </Editor>
          </Container>
        </FormControl>
      </Container>
    );
  }
}

class App extends React.Component {

  constructor(props) {
    super(props)
    this.codeA = React.createRef()
    this.codeB = React.createRef()
    this.codeBlockA = (<Block DefaultCodeName="代码 A" ref={this.codeA} />)
    this.codeBlockB = (<Block DefaultCodeName="代码 B" ref={this.codeB} />)
    this.listA = React.createRef()
    this.listB = React.createRef()
    this.FunctionListA = <FunctionList functions={Functions[0]} which={0} ref={this.listA} onReady={() => this.onReady()}></FunctionList>
    this.FunctionListB = <FunctionList functions={Functions[1]} which={1} ref={this.listB} onReady={() => this.onReady()}></FunctionList>
    this.decorationsA = []
    this.decorationsB = []
  }

  onReady() {
    if (Selected[0] != -1 && Selected[1] != -1) {
      const A = getFnPair(Functions[0][Selected[0]], Functions[1][Selected[1]])
      console.log(A)
      let newDecorationsA = []
      let newDecorationsB = []
      const zip = (a, b) => a.map((k, i) => [k, b[i]])
      for (const [LHS, RHS] of zip(A[0], A[1])) {
        if (LHS["node"] == RHS["node"]) {
          const [LR, RR] = [LHS["range"], RHS["range"]]
          newDecorationsA.push({
            range: new monaco.Range(LR["start"]["line"], LR["start"]["column"], LR["end"]["line"], LR["end"]["column"]),
            options: {
              isWholeLine: true,
              className: 'codeALine',
            }
          })
          newDecorationsB.push({
            range: new monaco.Range(RR["start"]["line"], RR["start"]["column"], RR["end"]["line"], RR["end"]["column"]),
            options: {
              isWholeLine: true,
              className: 'codeBLine',
            }
          })
        }
      }
      this.decorationsA = this.codeA.current.editor.current.getModel().deltaDecorations(this.decorationsA, newDecorationsA)
      this.decorationsB = this.codeB.current.editor.current.getModel().deltaDecorations(this.decorationsB, newDecorationsB)
    }
  }

  onSubmit() {
    let commandA = ''
    let commandB = ''
    const sourceCodeA = this.codeA.current.editor.current.getModel().getValue()
    const sourceCodeB = this.codeB.current.editor.current.getModel().getValue()
    this.codeA.current.getState((state) => {
      commandA = state.compileCommands
      this.codeB.current.getState((state) => {
        commandB = state.compileCommands
        // here...
      })
    })
  }

  onClear() {
    this.listA.current.setState({select: null})
    this.listB.current.setState({select: null})
    this.decorationsA = this.codeA.current.editor.current.getModel().deltaDecorations(this.decorationsA, [])
    this.decorationsB = this.codeB.current.editor.current.getModel().deltaDecorations(this.decorationsB, [])
    Selected = [-1, -1]
  }

  render() {
    return (
      <Container>
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Source Code Plagiarism Detection
              </Typography>
            </Toolbar>
          </AppBar>
        </Box>
        <Container sx={{
          marginTop: 4
        }}>
          <Card>
            <CardHeader title="提交源代码">
            </CardHeader>
            <CardContent>
              <Stack direction="row" spacing={2} divider={
                <Divider orientation="vertical" flexItem />
              } sx={{ flexGlow: 1 }}>
                {this.codeBlockA}
                {this.codeBlockB}
              </Stack>
              <Box textAlign="Center" sx={{ marginTop: 2 }}>
                <Button color="primary" margin="normal" onClick={() => this.onSubmit()}>
                  提交
                </Button>
              </Box>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  {this.FunctionListA}
                </Grid>
                <Grid item xs={6}>
                  {this.FunctionListB}
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Button color="primary" margin="normal" onClick={() => this.onClear()}>
                    清空
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Container>
      </Container>
    );
  }
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
