#pragma once

#include "llvm/Analysis/CFLAndersAliasAnalysis.h"
#include "llvm/Analysis/CFLSteensAliasAnalysis.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"

#include <memory>

#include "Pass/Instruction.hpp"

namespace DDG {

struct Graph
{
    enum DependenceEdgeType
    {
        Call,
        Control,
        DataGeneral,
        DataDefUse,
        DataRAW,
        DataRead,
        DataAlias,
        DataCallPara,
        Parameter,
        StructFields,
        NoDependency,
        GlobalDep,
    };

    struct Edge
    {
        InstructionNode to;
        std::shared_ptr<Edge> next;
        DependenceEdgeType type;

        // Clang 还没实现[聚合类的圆括号初始化](https://wg21.link/p1975): https://clang.llvm.org/cxx_status.html
        Edge(InstructionNode to, std::shared_ptr<Edge> next, DependenceEdgeType type) : to(to), next(next), type(type)
        {
        }
    };

    void addEdge(InstructionNode From, InstructionNode To, DependenceEdgeType Type)
    {
        EdgePool.emplace_back(std::make_shared<Edge>(To, Head[From].lock(), Type));
        Head[From] = EdgePool.back();
    }

    void setEntry(InstructionNode Node)
    {
        EntryMark.try_emplace(Node, true);
    }

    struct Iterator
    {
        std::weak_ptr<Edge> Current;

        Edge& operator*() const {
            assert(!Current.expired() && "dereferencing to an invalid pointer!");
            return *Current.lock();
        }

        Edge* operator->() const {
            assert(!Current.expired() && "dereferencing to an invalid pointer!");
            return Current.lock().get();
        }

        Iterator& operator++() {
            assert(!Current.expired() && "dereferencing to an invalid pointer!");
            Current = {Current.lock()->next};
            return *this;
        }

        Iterator operator++(int) {
            Iterator Origin = *this;
            operator++();
            return Origin;
        }

        bool operator==(const Iterator& RHS) const {
            return Current.lock() == RHS.Current.lock();
        }
    };

    Iterator begin_child(InstructionNode u) const {
        const auto It = Head.find(u);
        return It == Head.end() ? Iterator{} : Iterator{It->second};
    }

    Iterator end_child() const {
        return {};
    }

//    private:
    std::vector<std::shared_ptr<Edge>> EdgePool;
    llvm::DenseMap<InstructionNode, std::weak_ptr<Edge>> Head;
    llvm::DenseMap<InstructionNode, bool> EntryMark;
};

struct DataDependenceGraph : llvm::FunctionPass
{
    static char ID;

    DataDependenceGraph();

    virtual llvm::StringRef getPassName() const;

    virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const;

    virtual bool runOnFunction(llvm::Function& F);

   private:
    llvm::MemoryDependenceWrapperPass* MemoryDependencePass;
    llvm::CFLAndersAAWrapperPass* CFLAndersAAWrapperPass;
    llvm::CFLSteensAAWrapperPass* CFLSteensAAWrapperPass;
    Graph DDG;
};

}  // namespace DDG