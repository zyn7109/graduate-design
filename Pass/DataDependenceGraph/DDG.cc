#include "DDG.h"

#include "Pass/Instruction.hpp"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/STLFunctionalExtras.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/CFLAndersAliasAnalysis.h"
#include "llvm/Analysis/CFLSteensAliasAnalysis.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include "llvm/Analysis/MemoryLocation.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/WithColor.h"
#include "llvm/Support/raw_ostream.h"

#include <iterator>
#include <memory>
#include <type_traits>
#include <variant>

namespace DDG {

char DataDependenceGraph::ID = 0;

DataDependenceGraph::DataDependenceGraph() : llvm::FunctionPass(ID) {}

llvm::StringRef DataDependenceGraph::getPassName() const { return "DataDependenceGraph"; }

void DataDependenceGraph::getAnalysisUsage(llvm::AnalysisUsage& AU) const
{
    FunctionPass::getAnalysisUsage(AU);
    AU.addRequired<llvm::MemoryDependenceWrapperPass>();
    AU.addRequired<llvm::CFLAndersAAWrapperPass>();
    AU.addRequired<llvm::CFLSteensAAWrapperPass>();
    AU.setPreservesAll();
}

bool DataDependenceGraph::runOnFunction(llvm::Function& F)
{
    MemoryDependencePass = &getAnalysis<llvm::MemoryDependenceWrapperPass>();
    CFLAndersAAWrapperPass = &getAnalysis<llvm::CFLAndersAAWrapperPass>();
    CFLSteensAAWrapperPass = &getAnalysis<llvm::CFLSteensAAWrapperPass>();

    DDG = Graph();
    DDG.setEntry(F);

    auto& Anders = CFLAndersAAWrapperPass->getResult();
    auto& Steens = CFLSteensAAWrapperPass->getResult();
    auto& MD = MemoryDependencePass->getMemDep();

    const auto filterInstruction = [&F]<typename Type> {
        std::vector<Type*> Insts;
        llvm::transform(llvm::make_filter_range(llvm::instructions(F), [](auto&& I) { return llvm::isa<Type>(&I); }),
                        std::back_inserter(Insts),
                        [](llvm::Instruction& I) { return llvm::dyn_cast_or_null<Type>(&I); });
        return Insts;
    };

    const auto handleDefUseDependency = [&](llvm::Instruction& I) {
        for (auto& Op : I.operands()) {
            if (const auto Use = llvm::dyn_cast_or_null<llvm::Instruction>(&Op); Use) {
                DDG.addEdge(*Use, I, DDG::Graph::DataDefUse);
            }
        }
    };

    const auto handleCallDependency = [&](llvm::Instruction& I) {
        if (const auto Call = llvm::dyn_cast_or_null<llvm::CallInst>(&I); !Call)
            return;
        else {
            const auto Callee = Call->getCalledFunction();
            if (!Callee || (Callee && Callee->isDeclaration())) return;
            for (auto& Args : Call->args()) {
                if (const auto Use = llvm::dyn_cast_or_null<llvm::Instruction>(&Args); Use) {
                    DDG.addEdge(*Use, I, DDG::Graph::DataDefUse);
                }
            }
        }
    };

    const auto handleReadFromDependency = [&](llvm::Instruction& I) {
        if (const auto P = llvm::dyn_cast_or_null<llvm::LoadInst>(&I); P)
            DDG.addEdge(*llvm::dyn_cast<llvm::Instruction>(P->getPointerOperand()), I, DDG::Graph::DataRead);
    };

    const auto handleRAWDependency = [&](llvm::Instruction& I) {
        std::vector<llvm::Instruction*> RAW_DepsList;
        if (!llvm::isa<llvm::LoadInst>(&I)) return;
        {
            const auto LI = llvm::dyn_cast_or_null<llvm::LoadInst>(&I);
            const auto LI_ML = llvm::MemoryLocation::get(LI);
            llvm::transform(llvm::make_filter_range(
                                llvm::make_filter_range(llvm::instructions(F),
                                                        [](auto&& I) { return llvm::isa<llvm::StoreInst>(&I); }),
                                [&Anders, &Steens, &LI_ML](auto&& SI) {
                                    const auto SI_ML = llvm::MemoryLocation::get(&SI);
                                    const auto AAResult = Anders.query(LI_ML, SI_ML);
                                    const auto SteensResult = Steens.query(LI_ML, SI_ML);
                                    return AAResult != llvm::AliasResult::NoAlias ||
                                           SteensResult != llvm::AliasResult::NoAlias;
                                }),
                            std::back_inserter(RAW_DepsList), [](auto&& I) { return std::addressof(I); });
        }
        for (auto Ins : RAW_DepsList) {
            DDG.addEdge(*Ins, I, Graph::DataRAW);
        }
        return;
    };

    const auto handleNonLocalDependency = [&](llvm::Instruction& I) {
        llvm::SmallVector<llvm::NonLocalDepResult> Result;
        MD.getNonLocalPointerDependency(&I, Result);
        for (auto& Res : Result) {
            DDG.addEdge(I, *Res.getResult().getInst(), Graph::DataGeneral);
        }
    };

    // 1. Data dependency
    for (auto& I : llvm::instructions(F)) {
        handleDefUseDependency(I);
        handleCallDependency(I);

        if (llvm::isa<llvm::LoadInst>(I)) {
            handleReadFromDependency(I);
            handleRAWDependency(I);
            handleNonLocalDependency(I);
        }
    }

    // 2. Alias dependency
    // looks clumsy
    const auto& StoreInsts = filterInstruction.operator()<llvm::StoreInst>();
    const auto& LoadInsts = filterInstruction.operator()<llvm::LoadInst>();
    const auto& CastInsts = filterInstruction.operator()<llvm::CastInst>();

    // Store X Store; Store X Load
    for (auto SI : StoreInsts) {
        const auto SI_Loc = llvm::MemoryLocation::get(SI);
        for (auto LI : LoadInsts) {
            const auto LI_Loc = llvm::MemoryLocation::get(LI);
            const auto Result = Anders.query(SI_Loc, LI_Loc);
            if (Result != llvm::AliasResult::NoAlias) {
                DDG.addEdge(*LI, *SI, DDG::Graph::DependenceEdgeType::DataAlias);
                DDG.addEdge(*SI, *LI, DDG::Graph::DependenceEdgeType::DataAlias);
            }
        }

        for (auto si : StoreInsts) {
            if (SI == si) continue;
            const auto si_Loc = llvm::MemoryLocation::get(si);
            const auto Result = Anders.query(SI_Loc, si_Loc);
            if (Result != llvm::AliasResult::NoAlias) {
                DDG.addEdge(*SI, *si, DDG::Graph::DependenceEdgeType::DataAlias);
                DDG.addEdge(*si, *SI, DDG::Graph::DependenceEdgeType::DataAlias);
            }
        }
    }

    // Load X Load
    for (auto LI : LoadInsts) {
        for (auto li : LoadInsts) {
            if (LI == li) continue;
            if (LI->getPointerOperandType() != li->getPointerOperandType()) continue;
            const auto LI_Loc = llvm::MemoryLocation::get(LI);
            const auto li_Loc = llvm::MemoryLocation::get(li);
            const auto Result = Anders.query(LI_Loc, li_Loc);
            if (Result != llvm::AliasResult::NoAlias) {
                DDG.addEdge(*LI, *li, Graph::DataAlias);
                DDG.addEdge(*li, *LI, Graph::DataAlias);
            }
        }
    }

    // Cast X Cast
    for (auto CI : CastInsts) {
        const auto Src = llvm::dyn_cast<llvm::Instruction>(CI->getOperand(0));
        DDG.addEdge(*Src, *CI, Graph::DataAlias);
        DDG.addEdge(*CI, *Src, Graph::DataAlias);
    }

    llvm::WithColor::note() << DDG.Head.size() << "\n";

    llvm::DenseMap<InstructionNode, bool> Visited;
    llvm::function_ref<void(InstructionNode)> DebugDump = [&](InstructionNode I) {
        static auto Visitor = llvm::makeVisitor(
            [](llvm::Instruction* I) { if (I) llvm::WithColor(llvm::outs(), llvm::raw_ostream::Colors::MAGENTA) << *I; },
            [](llvm::Function* F) { if (F) llvm::WithColor(llvm::outs(), llvm::raw_ostream::Colors::YELLOW) << *F; },
            [](InstructionNode::empty_t) {}, [](InstructionNode::tombstone_t) {});
        if (Visited.find(I) != Visited.end()) return;
        Visited[I] = true;
        llvm::outs() << "\"";
        std::visit(Visitor, I.Storage);
        llvm::outs() << "\";\n";
        for (auto It = DDG.begin_child(I), E = DDG.end_child(); It != E; ++It) {
            llvm::outs() << "\"";
            std::visit(Visitor, I.Storage);
            llvm::outs() << "\" -> ";
            llvm::outs() << "\"";
            std::visit(Visitor, It->to.Storage);
            llvm::outs() << "\" " << llvm::formatv("[label={0}];\n", static_cast<int32_t>(It->type));
            DebugDump(It->to);
        }
    };

    llvm::outs() << llvm::formatv("digraph \"{0}\"{\n", F.getName());
    if (DDG.Head.size()) {
        DebugDump(DDG.Head.begin()->getFirst());
    }
    llvm::outs() << "}\n";
    return false;  // false for read-only
}

}  // namespace DDG

llvm::RegisterPass<DDG::DataDependenceGraph> Register("ddg", "DDG", false, true);
