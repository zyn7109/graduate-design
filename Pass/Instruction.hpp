#pragma once

#include "llvm/ADT/DenseMapInfo.h"
#include "llvm/ADT/GraphTraits.h"
#include "llvm/ADT/None.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Pass.h"
#include "llvm/Support/DOTGraphTraits.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/GraphWriter.h"
#include "llvm/Support/WithColor.h"
#include "llvm/Support/raw_ostream.h"

#include <variant>

struct GraphType;

struct InstructionNode
{
    InstructionNode(llvm::Instruction& I, GraphType* G = nullptr) : Storage(&I), G(G) {}
    InstructionNode(llvm::Function& F, GraphType* G = nullptr) : Storage(&F), G(G) {}

    struct empty_t
    {
        bool operator==(const empty_t& RHS) const { return RHS.Val == Val; }
        void* Val;
    };
    struct tombstone_t
    {
        bool operator==(const tombstone_t& RHS) const { return RHS.Val == Val; }
        void* Val;
    };

    InstructionNode(empty_t E) : Storage(E) {}
    InstructionNode(tombstone_t T) : Storage(T) {}

    GraphType* G = nullptr;
    std::variant<llvm::Instruction*, llvm::Function*, empty_t, tombstone_t> Storage;
};

template <>
struct llvm::DenseMapInfo<InstructionNode>
{
    static inline InstructionNode getEmptyKey()
    {
        return {InstructionNode::empty_t{llvm::DenseMapInfo<void*>::getEmptyKey()}};
    }

    static inline InstructionNode getTombstoneKey()
    {
        return {InstructionNode::tombstone_t{llvm::DenseMapInfo<void*>::getTombstoneKey()}};
    }

    static unsigned getHashValue(const InstructionNode& Val)
    {
        void* PtrVal = nullptr;
        const auto Visitor = llvm::makeVisitor([&PtrVal](llvm::Function* F) { PtrVal = F; },
                                               [&PtrVal](llvm::Instruction* I) { PtrVal = I; },
                                               [&PtrVal](InstructionNode::empty_t E) { PtrVal = E.Val; },
                                               [&PtrVal](InstructionNode::tombstone_t T) { PtrVal = T.Val; });
        std::visit(Visitor, Val.Storage);
        return llvm::DenseMapInfo<void*>::getHashValue(PtrVal);
    }

    static bool isEqual(const InstructionNode& LHS, const InstructionNode& RHS) { return LHS.Storage == RHS.Storage; }
};

struct GraphType : llvm::DenseMap<InstructionNode, llvm::DenseSet<InstructionNode>>
{
    using NodeRef = std::add_pointer_t<const InstructionNode>;

    static GraphType::value_type::second_type& getEmptyList()
    {
        static GraphType::value_type::second_type Ret;
        return Ret;
    }

    struct EdgeRef
    {
        NodeRef U;
        NodeRef V;
        friend bool operator==(const EdgeRef& LHS, const EdgeRef& RHS) { return LHS.U == RHS.U && LHS.U == RHS.U; }
    };

    struct EdgesIterator
    {
        llvm::Optional<EdgeRef> Edge;
        EdgeRef operator*() const { return *Edge; }
        EdgesIterator operator++() const
        {
            const auto& G = Edge->U->G;
            const auto It = G->find(*(Edge->U));
            if (It != G->end()) {
                auto Iter = It->second.find(*(Edge->V));
                if (++Iter == It->second.end()) return {};
                return {EdgeRef{Edge->U, &*Iter}};
            }
            return {};
        }
        EdgesIterator operator++(int) const
        {
            const auto Last = *this;
            operator++();
            return Last;
        }
        friend bool operator==(const EdgesIterator& LHS, const EdgesIterator& RHS) { return LHS.Edge == RHS.Edge; }
    };

    EdgesIterator getChildEdges_begin(NodeRef N) const
    {
        auto It = this->find(*N);
        if (It == this->end()) {
            return {};
        }
        auto Iter = It->second.begin();
        return {EdgeRef{N, &*Iter}};
    }

    EdgesIterator getChildEdges_end(NodeRef N) const { return {}; }

    std::set<NodeRef>& getFlatNodes() const
    {
        static std::set<NodeRef> Set;
        Set.clear();
        for (auto& E : *this) {
            Set.emplace(&E.first);
            for (auto& V : E.second) Set.emplace(&V);
        }
        return Set;
    }
};

template <>
struct llvm::GraphTraits<GraphType>
{
    using NodeRef = GraphType::NodeRef;
    using ChildIteratorType = std::vector<NodeRef>::iterator;

    static NodeRef getEntryNode(const GraphType& G) { return &G.begin()->getFirst(); }

    static ChildIteratorType child_begin(NodeRef Node)
    {
        const auto It = Node->G->find(*Node);
        if (It == Node->G->end()) return {};
        static std::vector<NodeRef> V;
        V.clear();
        for (auto vv : It->second) V.emplace_back(&vv);
        return V.begin();
    }

    static ChildIteratorType child_end(NodeRef Node)
    {
        const auto It = Node->G->find(*Node);
        if (It == Node->G->end()) return {};
        static std::vector<NodeRef> V;
        V.clear();
        for (auto vv : It->second) V.emplace_back(&vv);
        return V.end();
    }

    // typedef  ...iterator nodes_iterator; - dereference to a NodeRef

    using nodes_iterator = std::set<NodeRef>::iterator;
    static nodes_iterator nodes_begin(const GraphType& G) { return G.getFlatNodes().begin(); }

    static nodes_iterator nodes_end(const GraphType& G) { return G.getFlatNodes().end(); }

    // typedef EdgeRef           - Type of Edge token in the graph, which should
    //                             be cheap to copy.
    using EdgeRef = GraphType::EdgeRef;
    using ChildEdgeIteratorType = GraphType::EdgesIterator;
    // typedef ChildEdgeIteratorType - Type used to iterate over children edges in
    //                             graph, dereference to a EdgeRef.

    //     Return iterators that point to the beginning and ending of the
    //     edge list for the given callgraph node.
    //
    static ChildEdgeIteratorType child_edge_begin(NodeRef N) { return N->G->getChildEdges_begin(N); }

    static ChildEdgeIteratorType child_edge_end(NodeRef N) { return N->G->getChildEdges_end(N); }

    //     Return the destination node of an edge.
    static NodeRef edge_dest(EdgeRef E) { return E.V; }

    //    Return total number of nodes in the graph
    static unsigned size(GraphType* G) { return G->getFlatNodes().size(); }
};

template <>
struct llvm::DOTGraphTraits<GraphType> : llvm::DefaultDOTGraphTraits
{
    DOTGraphTraits() = default;
    DOTGraphTraits(bool) {}

    /// isNodeHidden - If the function returns true, the given node is not
    /// displayed in the graph.
    template <typename GraphType>
    static bool isNodeHidden(const auto&, const GraphType&)
    {
        return false;
    }

    template <typename G>
    static std::string getNodeIdentifierLabel(const GraphType::NodeRef Node, const G&)
    {
        std::string Ret;
        auto Visitor = llvm::makeVisitor([&Ret](llvm::Instruction* I) { Ret = llvm::formatv("{}", I); },
                                         [&Ret](llvm::Function* F) { Ret = llvm::formatv("{}", F); },
                                         [](InstructionNode::empty_t) {}, [](InstructionNode::tombstone_t) {});
        std::visit(Visitor, Node->Storage);
        return "\"" + Ret + "\"";
    }
};
