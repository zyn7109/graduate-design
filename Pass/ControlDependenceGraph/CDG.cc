// Implementation of _The Program Dependence Graph and Its Use in Optimization_
// 3.1.1 Determining Control Dependence

#include "CDG.h"
#include <type_traits>

template <typename T>
concept InstructionLike =
    std::same_as<std::remove_cvref_t<T>, llvm::Instruction> || std::same_as<std::remove_cvref_t<T>, llvm::Function>;

namespace CDG {

ControlDependenceGraph::ControlDependenceGraph() : llvm::FunctionPass(ID) {}

llvm::StringRef ControlDependenceGraph::getPassName() const { return "ControlDependenceGraph"; }

void ControlDependenceGraph::getAnalysisUsage(llvm::AnalysisUsage& AU) const
{
    FunctionPass::getAnalysisUsage(AU);
    AU.addRequired<llvm::PostDominatorTreeWrapperPass>();
    AU.setPreservesAll();
}

bool ControlDependenceGraph::runOnFunction(llvm::Function& F)
{
    if (F.isDeclaration()) return false;
    // 这里注意 PostDominatorTreeWrapperPass 是一个 FunctionPass
    // 如果写成 getAnalysis(F) 会调用 PMDataManager::getOnTheFlyPass 造成 UB
    PostDomTree = &getAnalysis<llvm::PostDominatorTreeWrapperPass>();
    const auto& PDT = PostDomTree->getPostDomTree();

    auto CDG = std::make_shared<GraphType>();
    // CDG.try_emplace(F, llvm::SetVector<InstructionNode>());

    static auto AddDependenceInst2Block = [&](InstructionLike auto& From, llvm::BasicBlock& BB, auto& Graph) {
        // 枚举 BB 中的每一条指令
        llvm::for_each(BB, [&](auto& I) { Graph[InstructionNode{From, &Graph}].insert(InstructionNode{I, &Graph}); });
    };

    const auto EntryBBNode = PDT.getNode(&F.getEntryBlock());
    // 遍历支配树，找节点的直接支配点
    for (auto N = EntryBBNode; N && N->getBlock(); N = N->getIDom()) {
        AddDependenceInst2Block(F, *N->getBlock(), *CDG);
    }

    llvm::SetVector<std::pair<llvm::BasicBlock*, llvm::BasicBlock*>> EdgeSet;

    for (auto& BB : F) {
        for (auto SuccBB : llvm::successors(&BB)) {
            assert(SuccBB && "SuccBB is nullptr");
            if (!PDT.dominates(SuccBB, &BB)) {
                EdgeSet.insert({&BB, SuccBB});
            }
        }
    }

    static auto AddDependencyBlock2Block = [&](llvm::BasicBlock& A, llvm::BasicBlock& B, auto& Graph) {
        const auto From = A.getTerminator();
        // if (&A != &B) {

        AddDependenceInst2Block(*From, B, Graph);
        // } else {
        //     AddDependenceInst2Block(*From, A, Graph);
        // }
    };

    for (auto& E : EdgeSet) {
        const auto L = PDT.findNearestCommonDominator(E.first, E.second);
        auto A = E.first, B = E.second;
        // case 1
        //    L = parent of A. All nodes in the post-dominator tree on the path
        // from L to B, including B but not L, should be made control dependent on A.

        for (auto b = PDT.getNode(B); b && b->getBlock() != L; b = b->getIDom()) {
            AddDependencyBlock2Block(*A, *(b->getBlock()), *CDG);
        }

        // case 2
        // L = A. All nodes in the post-dominator tree on the path from A to B,
        // including A and B, should be made control dependent on A. (This case captures
        // loop dependence.)
        if (L == A) AddDependencyBlock2Block(*A, *A, *CDG);
    }

    G = CDG;
    // DumpDOT(*CDG);
    static auto Visitor = llvm::makeVisitor([](llvm::Instruction* I) { llvm::errs() << "\"" << I << "\""; },
                                            [](llvm::Function* F) { llvm::errs() << "\"" << F << "\""; },
                                            [](InstructionNode::empty_t) {}, [](InstructionNode::tombstone_t)
                                            {});
    // llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::RED) << F.getName() << "\n";
    llvm::errs() << llvm::formatv("digraph \"{0}\"{\n", F.getName());
    for (auto& Edge : *CDG) {
        // llvm::errs() << "Node: ";
        // llvm::WithColor(llvm::errs(), llvm::raw_ostream::Colors::GREEN) << "Node: ";
        std::visit(Visitor, Edge.first.Storage);
        llvm::errs() << ";\n";
        for (auto& V : Edge.second) {
            std::visit(Visitor, Edge.first.Storage);
            llvm::errs() << " -> ";
            std::visit(Visitor, V.Storage);
            llvm::errs() << ";\n";
        }
    }
    llvm::errs() << "}\n";
    return false;  // readonly
}

void ControlDependenceGraph::DumpDOT(const GraphType& G) const
{
    llvm::GraphWriter<GraphType> Writer{llvm::errs(), G, false};
    Writer.writeGraph("Test");
}

}  // namespace CDG

char CDG::ControlDependenceGraph::ID = 0;

llvm::RegisterPass<CDG::ControlDependenceGraph> R("cdg", "CDG", false, true);
