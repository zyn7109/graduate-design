#pragma once

#include "Pass/Instruction.hpp"

#include "llvm/ADT/DenseMapInfo.h"
#include "llvm/ADT/GraphTraits.h"
#include "llvm/ADT/None.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/PostDominators.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/Pass.h"
#include "llvm/Support/DOTGraphTraits.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/GraphWriter.h"
#include "llvm/Support/WithColor.h"
#include "llvm/Support/raw_ostream.h"

#include <variant>

namespace CDG {

struct ControlDependenceGraph : llvm::FunctionPass
{
    static char ID;

    ControlDependenceGraph();

    virtual llvm::StringRef getPassName() const;

    virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const;

    virtual bool runOnFunction(llvm::Function& F);

    void DumpDOT(const GraphType& G) const;

   private:
    llvm::PostDominatorTreeWrapperPass* PostDomTree;
    std::shared_ptr<GraphType> G;
};

}  // namespace CDG
